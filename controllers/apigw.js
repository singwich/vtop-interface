var request = require('request');
var util = require('util');
var config = require('../common/config');
var log = require('../common/logging');
var db = require("../common/database");

function topUp(args, req, res) {
    try {
        var startTime = new Date().getTime();
        var {refNumber, date, type, amount, requestId, requestTime} = args;
        var url = config.get('vtop.topup_url');
        var timeout = config.get('vtop.topup_timeout');

        if (type == "reversal") {
            url = config.get('vtop.reversal_url');
            timeout = config.get('vtop.reversal_timeout');
        }
        var body = util.format('refno=%s&date=%s&amount=%s&notes=%s', refNumber, date, amount, type);
        log.info(util.format('topUp|requestId=%s|refNumber=%s|type=%s|amount=%s|data=%s', requestId, refNumber, type, amount, body));

        // call acs
        callAPIGW({
                operation: 'topup',
                url:  url,
                timeout: timeout,
                body: body,
                requestTime: requestTime,
                requestId: requestId
            },
            function (err, response, body) {
                var status = body.trim();
                console.log(util.format("res => '%s', length = %s", status, status.length));

                if (err) {
                    console.log(err);

                    var result = {status: "nok", results: {message: "Request Failed. Please contact the administrator"}};

                    log.error(util.format('!doTopup|requestId=%s|cost=%dms|result=%s', requestId, timeDiff(startTime), err));
                    log.error(util.format('!doTopup|requestId=%s|cost=%dms|result=%s', requestId, timeDiff(requestTime), err))

                    res.render('topup.html', {html_head_title: config.get('service.html_head_title'),
                        error_msg: "Request Failed. Please contact the administrator",
                        user_menus: req.session.pageMenus,
                        username: req.session.userDetails.username,
                        first_name: req.session.userDetails.first_name,
                        user_menus: req.session.pageMenus,
                        hidden: true,
                        reference_options: ''});

                } else if (status == "OK") {

                        var result = {status: "ok", results: body};
                        log.info(util.format('doTopup|requestId=%s|cost=%dms|result=%s', requestId, timeDiff(requestTime), result));

                        // update top up request status

                        var sql = util.format("UPDATE public.tbl_vtop_topup_requests SET status=1 WHERE record_id=%s::bigint", req.session.topup_approval_info.record_id);

                        console.log(sql);

                        var updateResults = db.executeQuery(requestId,
                            sql,
                            function (err) {
                                console.log(err);
                                log.error(util.format("REQ Status update failed|error=%s", error));

                                res.render('topup.html', {html_head_title: config.get('service.html_head_title'),
                                    error_msg: "The topup was successfully!",
                                    username: req.session.userDetails.username,
                                    first_name: req.session.userDetails.first_name,
                                    user_menus: req.session.pageMenus,
                                    hidden: true,
                                    reference_options: ''});
                            },
                            function (result) {
                                log.info(util.format("REQ status Updated|REQInfo=%s", req.session.topup_approval_info));

                                res.render('topup.html', {html_head_title: config.get('service.html_head_title'),
                                    error_msg: "The topup was successfully!",
                                    username: req.session.userDetails.username,
                                    first_name: req.session.userDetails.first_name,
                                    user_menus: req.session.pageMenus,
                                    hidden: true,
                                    reference_options: ''});
                            });

                    } else {
                        var result = {
                            status: "nok",
                            results: body
                        };

                        log.error(util.format('!doTopup|requestId=%s|cost=%dms|result=%s', requestId, timeDiff(requestTime), result));
                        res.render('topup.html', {html_head_title: config.get('service.html_head_title'),
                            error_msg: "Request Failed. Please contact the administrator",
                            username: req.session.userDetails.username,
                            first_name: req.session.userDetails.first_name,
                            user_menus: req.session.pageMenus,
                            hidden: true,
                            reference_options: ''});

                    }


            });

    } catch (e) {
        log.error('!doTopup: ', e);

        res.render('error.html', {
            error: "Request failed",
            result: 'Request Failed. Please contact the administrator.'
        });
    }
}

function callAPIGW(args, callback) {
    // get request parameters
    var {operation, url, body, requestId, requestTime, sessionId, msisdn, timeout} = args;
    log.info(util.format("callVTOP|requestId=%s|op='%s'|url=%s|data='%s')", requestId, operation, url, body));

    // http options
    var options = {
        method: 'post',
        body: body,
        url: util.format("%s?%s", url, body),
        timeout: timeout,
        headers: {
            "content-type": "text/plain"
        }
    }

    var startTime = new Date().getTime();
    request(options, function (error, response, body) {
        log.info(util.format('callVTOP|requestId=%s|cost=%dms|result=%s', requestId, timeDiff(startTime), body));

        if (error) {
            var errorMessage = error.message;
            if (error.code == 'ETIMEDOUT') {
                errorMessage = (error.connect === true) ? 'connectionTimeout' : 'readTimeout';
            }

            log.error(util.format("!callVTOP|requestId=%s|errorCode=%s|errorMsg='%s'", requestId, error.code, errorMessage))
        }

        callback(error, response, body);
    });
}

function timeDiff(time) {
    var now = new Date().getTime();
    return (now - time);
}

//
module.exports = {topUp}