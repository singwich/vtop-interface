// globals
$.ajaxSetup({cache: false});
//$.getScript("javascripts/mustache.js", function() {});

window.table = null;

// DO GET
function ajaxGet(path, successCallback, errorCallback) {
    var rootURL = window.location.protocol + "//" + window.location.host + path;
    //var search =  ? "" : "subscriber_no=" + subscriber;
    var url = rootURL;
    //url += (window.location.search == '') ? "?" + sub : window.location.search + sub;
    var urlRand = new Date().getTime(); //used to prevent ie caching

    $.ajax({
        cache: false,
        crossDomain: true,
        type: "GET",
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        url: url,
        success: function (result) {
            //console.log("Success: ", result);
            successCallback(result);
        },
        error: function (e) {
            errorCallback(e);
            $("#offers").html("<strong>Error:</strong> " + e);
            //window.location = rootURL + '/error';
        }
    });
}

function renderFloatTopUps(data) {

}

function RoundedYMax(maxVal) {
    if (maxVal == 0) {return 0};
    var size = Math.floor(Math.log(Math.abs(maxVal)) / Math.LN10);
    var magnitude = Math.pow(10, size);
    var yMax = Math.ceil(maxVal / magnitude) * magnitude;
    return yMax;
}

function renderFloatRecycles(data) {
    var recycleLabels = [];
    var recycleAmounts = [];
    var maxAmount = 0;
    var maxTick = 1000000;

    $.each(data, function (i, recycle) {
        recycleLabels[i] = recycle.recycle_date;
        recycleAmounts[i] = recycle.recycle_amount;

        // find maximum recycle amount
        maxAmount = Math.max(maxAmount, recycle.recycle_amount);
    });

    // compute the max ceiling
    maxTick = RoundedYMax(maxAmount);

    // Bar Chart Example
    $('#id-float-recycles').empty();
    $('#id-float-recycles').append('<canvas id="floatRecycles"></canvas>');

    var ctx = document.getElementById("floatRecycles");
    var myBarChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: recycleLabels,
            datasets: [{
                label: "Recycle",
                backgroundColor: "yellow",
                hoverBackgroundColor: "#d9c82e",
                borderColor: "#4e73df",
                data: recycleAmounts,
            }],
        },
        options: {
            maintainAspectRatio: false,
            layout: {
                padding: {
                    left: 10,
                    right: 25,
                    top: 25,
                    bottom: 0
                }
            },
            scales: {
                xAxes: [{
                    time: {
                        unit: 'day'
                    },
                    gridLines: {
                        display: true,
                        drawBorder: false
                    },
                    ticks: {
                        maxTicksLimit: 10
                    },
                    maxBarThickness: 25,
                }],
                yAxes: [{
                    ticks: {
                        min: 0,
                        max: maxTick,
                        maxTicksLimit: 7,
                        padding: 10,
                        fontSize: 12,
                        // Include a dollar sign in the ticks
                        callback: function(value, index, values) {
                            return '' + number_format(value);
                        }
                    },
                    gridLines: {
                        color: "rgb(234, 236, 244)",
                        zeroLineColor: "rgb(234, 236, 244)",
                        drawBorder: false,
                        borderDash: [2],
                        zeroLineBorderDash: [2]
                    },
                    scaleLabel: {
                        display: true,
                        labelString: 'Amount (Pula)',
                        fontSize: 16
                    }
                }],
            },
            legend: {
                display: false
            },
            tooltips: {
                titleMarginBottom: 10,
                titleFontColor: '#6e707e',
                titleFontSize: 14,
                backgroundColor: "rgb(255,255,255)",
                bodyFontColor: "#858796",
                borderColor: '#dddfeb',
                borderWidth: 1,
                xPadding: 15,
                yPadding: 15,
                displayColors: false,
                caretPadding: 10,
                callbacks: {
                    label: function(tooltipItem, chart) {
                        var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                        return datasetLabel + ': ' + number_format(tooltipItem.yLabel);
                    }
                }
            },
        }
    });

}

function renderFloatBalance(data) {
    var float = data[0];
    let float_level = parseFloat(float.float_level);
    var current_balance = float.current_balance;

    //
    $('#current_float').empty();
    $('#float_topups').empty();
    $('#current_float').append(current_balance);
    $('#float_topups').append(float.float_topups);


    // render the Gauge
    JSC.chart('myChartDiv',{
        debug: false,
        legend_visible: false,
        defaultTooltip_enabled: false,
        xAxis_spacingPercentage: 0.5,
        background: '#343a40',
        style: { color: '#343a40'},
        fill: '#343a40',
        color: '#343a40',
        yAxis: [
            {
                id: 'ax1',
                defaultTick: {  padding: 10,  enabled: false},
                customTicks: [  0, 10, 40, 60, 100],
                line: {
                    width: 10,

                    /*Defining the option will enable it.*/
                    breaks: {},

                    /*Palette is defined at series level with an ID referenced here.*/
                    color: 'smartPalette:pal1'
                },
                scale_range: [  0,  100]
            }
        ],
        defaultSeries: {
            type: 'gauge column roundcaps',
            fill: '#343a40',
            shape: {
                label: {
                    text: '%value%',
                    align: 'center',
                    verticalAlign: 'middle'
                }
            }
        },
        series: [
            {
                type: 'column roundcaps',
                name: 'M\'Bootse Current Float vs Top Ups Made',
                color: '#343a40',
                yAxis: 'ax1',
                visible: true,
                palette: {
                    id: 'pal1',
                    pointValue: '%yValue',
                    ranges: [
                        {value: 0,color: '#c30101' },
                        {value: 10,color: '#ff5252' },
                        {value: 40,color: '#FFD221' },
                        {value: 60,color: '#77E6B4' },
                        {value: 100,color: '#21D683' }
                    ]
                },
                shape_label: {  style: { fontSize: 32, color: '#77E6B4'}},
                points: [  [ 'x', float_level  ]]
            }
        ]
    });
}

function loadBalances(date) {
    var queryString = '';

    if (date != '') {
        queryString = '?date=' + date;
    }

    // balance
    ajaxGet('/vtop/get_balance' + queryString,
        function (result) {

            if (result.status == "OK") {
                renderFloatBalance(result.data);
            }
            else {
                renderFloatBalance(null);
            }
        },
        function (error) {
            console.log(error);
        });
}

function loadRecycles (from, to) {
    var queryString = '';

    if (from != '' && to != '') {
        queryString = '?from=' + from + '&to=' + to;
    }

    // load reports
    ajaxGet('/vtop/get_recycles' + queryString,
        function (result) {

            if (result.status == "OK") {
                renderFloatRecycles(result.data);
            }
            else {
                renderFloatRecycles(null);
            }
        },
        function (error) {
            console.log(error);
        });
}

function loadTopUps (from, to) {
    var queryString = '';

    if (from != '' && to != '') {
        queryString = '?from=' + from + '&to=' + to;
    }

    if (window.table) {
        window.table.destroy();
    }

    window.table = $('#id-floatTopUps').DataTable ({
        "processing": true,
        "serverSide": true,
        "ajax": {url: '/vtop/get_topups' + queryString,
            type: 'GET'
        },
        "columns": [
            { "data": "topup_date"},
            { "data": "reference_no" },
            { "data": "amount", render: $.fn.dataTable.render.number( ',', '.', 2, '' )},
            { "data": "notes" }],
        dom : 'lBfrtip',
        buttons:[ { extend: 'excel',
            text: ' Export as EXCEL',
            filename: function(){
                var d = new Date();
                var n = d.getTime();
                return "Float_Topups_" + n;
            },
        title: "M'Booste Float Topups"} ]
    });

    window.table.buttons().container()
        .appendTo('btn button-primary');
}

$(document).ready(function () {
    var fromDate = $("#dateFrom").val();
    var toDate = $("#dateTo").val();

    loadRecycles(fromDate,toDate);
    loadTopUps(toDate,toDate);
    loadBalances(toDate);
    //*/

    // datepicker settings
    var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
    var options={
        format: 'yyyy-mm-dd',
        container: container,
        todayHighlight: true,
        autoclose: true
    };

    // date from
    $('#dateFrom').datepicker(options);

    // date to
    $('#dateTo').datepicker(options);
});

$(document).on('click', '#btn-filter', function (e) {
    console.log('filter button clicked');
    var fromDate = $("#dateFrom").val();
    var toDate = $("#dateTo").val();

    if (fromDate == '' ) {
        alert ("Select the From Date");
    }
    else if (toDate == '') {
        alert ("Select the To Date");
    }
    else {
        loadTopUps(fromDate, toDate);
        loadRecycles(fromDate, toDate);
        loadBalances(toDate);
    }

});

$(document).on('change', '#ref_no', function (e) {
    var option = $(this).children(":selected").attr("id");;

    if (option != '') {
        ajaxGet('/vtop/get_approval_info?record_id=' + option,
            function (result) {

                if (result.status == "OK") {
                    var info = result.data[0];
                    var amount = info.amount;

                    $("#amount").empty();
                    $("#amount").val(amount);
                }
                else {

                }
            },
            function (error) {
                console.log(error);
            });
    }

});

// password validation

$(document).on('keyup', '#password_confirmation, #password', function (e) {

    if ($('#password').val() == $('#password_confirmation').val()) {
        $('#message').html('Passwords Match').css('color', 'green');

    } else
        $('#message').html('Passwords do not Match').css('color', 'red');
});

$(document).on('submit', '#change-password-form', function (e) {
     if ($('#password').val() != $('#password_confirmation').val()) {
         $('#message').html('Passwords do not Match').css('color', 'red');
         e.preventDefault();
    }

});

$(document).on('submit', '#btn-submit-approval', function (e) {
    if ($('#amount').val() == '' || $('#ref_no').val() == '' ) {
        $('#error-msg').append('Please fill in the Ref No and Amount');
        e.preventDefault();
    }

});
