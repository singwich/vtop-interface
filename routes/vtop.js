var express = require('express');
var path = require('path');
var appRoot = require('app-root-path');
var util = require('util');
var apigw = require('../controllers/apigw');
var log = require('../common/logging');
var session = require('../common/session');
var config = require('../common/config');
var db = require("../common/database");
var rn = require('random-number');
var querystring = require("querystring");
var crypto = require('crypto');
var dateFormat = require('dateformat');

var gen = rn.generator({
    min:  1
    , max:  9999
    , integer: true
})

const { base64encode, base64decode } = require('nodejs-base64');

var viewsPath = path.join(__dirname, 'views');

var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    //var isValid, errorMsg, redirect; ({isValid, errorMsg, redirect} = session.validateSession(req));

    if (req.session.userDetails == null) {

        res.render('login.html', {html_head_title: config.get('service.html_head_title'), error_msg: ""});
    }
    else if (req.session.userDetails != null) {
        var userDetails = req.session.userDetails;
        res.render('index.html',
            {html_head_title: config.get('service.html_head_title'),
                username: userDetails.username,
                user_id: req.session.userDetails.user_id,
                first_name: userDetails.first_name,
                middle_name: userDetails.middle_name,
                last_name: userDetails.last_name,
                last_login: userDetails.last_login,
                user_menus: req.session.pageMenus
            });
    }
    else{
        res.render('error.html', {html_head_title: config.get('service.html_head_title')});
    }
});

// login
router.get('/login', function(req, res, next) {
    if (res.session == null) {
        res.render('login.html', {html_head_title: config.get('service.html_head_title'),
            error_msg: ""});
    }
    else {

    }
});

// get data
router.post('/login', function(req, res, next) {

    if (req.query != null) {
        var startTime = new Date().getTime();
        var username = req.body.username;
        var password = req.body.password;

        // check if session exists else return error.
        log.info(util.format('login|username=%s', username));

        var sql = util.format("select t1.user_id, t2.role_id, username, first_name, middle_name, last_name, status, last_login \n" +
            "from tbl_users t1 join tbl_user_roles t2 \n" +
            "on t1.user_id = t2.user_id  where username = '%s' and upper(password) = upper('%s')", querystring.escape(username), crypto.createHash('md5').update(querystring.escape(password)).digest('hex'));

        var results = db.executeQuery(1,
            sql,
            function (err) {
                console.log(err);
                res.render('login.html', {html_head_title: config.get('service.html_head_title'), error_msg: "invalid username/password"});
            },
            function (result) {
                //console.log(result);

                var userDetails = result.rows[0];

                if (userDetails) {
                    if (userDetails.status != 1) {
                        res.render('login.html', {html_head_title: config.get('service.html_head_title'),
                            error_msg: "Sorry, the User is not active!"});
                    }
                    else {
                        // set session details
                        req.session.userDetails = userDetails;
                        req.session.pageMenus = '';
                        req.session.userLinks = [];

                        // fetch pages based on role
                        db.executeQuery(1,
                            util.format("select role_id, rp.permission_id, resource, resource_description from tbl_role_permissions rp join tbl_permissions p \n" +
                                "on rp.permission_id = p.permission_id\n" +
                                "where role_id = '%s'::integer\n" +
                                "order by rp.permission_id asc", userDetails.role_id),
                            function (err) {
                                res.render('login.html', {html_head_title: config.get('service.html_head_title'),
                                    error_msg: "An error occured while trying to fetch pages.!"});
                            },
                            function (page_results) {
                                var pageMenus = '';
                                for (let i in page_results.rows) {
                                    var pageInfo = page_results.rows[i];
                                    pageMenus += util.format('<a class="collapse-item" href="%s">%s</a>', pageInfo.resource, pageInfo.resource_description);
                                    req.session.userLinks[i] = pageInfo.resource;
                                }

                                req.session.pageMenus = pageMenus;

                                // update lasr login time
                                db.executeQuery(1,
                                    util.format("UPDATE public.tbl_users SET last_login = 'now()'::timestamp without time zone WHERE user_id = '%s'::integer", userDetails.user_id),
                                    function (err) {
                                        res.render('login.html', {html_head_title: config.get('service.html_head_title'),
                                            error_msg: "An error occured while trying to login.!"});
                                    },
                                    function (reslt) {
                                        res.redirect('/vtop');
                                    });
                            });
                        // end of fetch pages
                    }
                }
                else {
                    res.render('login.html', {html_head_title: config.get('service.html_head_title'),
                    error_msg: "Invalid Username/password"});
                }
            });
    }
    else {
        // get parameters
        res.render('login.html', {html_head_title: config.get('service.html_head_title'), error_msg: ""});
    }
});

router.get('/approval', function(req, res, next) {
    //var isValid, errorMsg, redirect; ({isValid, errorMsg, redirect} = session.validateSession(req));

    if (req.session.userDetails == null) {

        res.render('login.html', {html_head_title: config.get('service.html_head_title'), error_msg: ""});
    }
    else if (req.session.userLinks.includes(req.originalUrl)) {

        res.render('approval.html', {html_head_title: config.get('service.html_head_title'),
            error_msg: "",
            username: req.session.userDetails.username,
            first_name: req.session.userDetails.first_name,
            user_menus: req.session.pageMenus});
    }
    else{

        // take him back home
        res.redirect('/vtop');
    }
});

router.post('/approval', function(req, res, next) {
    var startTime = new Date().getTime();
    var random = util.format('%s', gen());
    var refNumber = req.body.ref_no;
    var amount = req.body.amount;
    var requestId = util.format('%d%d', startTime, random.padStart(4,0));

    if (req.session.userDetails == null) {
        res.render('login.html', {html_head_title: config.get('service.html_head_title'), error_msg: ""});
    }
    else if (refNumber != null && amount != null ){
        var sql = util.format("INSERT INTO public.tbl_vtop_topup_requests(reference_no, amount, entered_by) VALUES ('%s'::text, %s::numeric, '%s'::text) returning record_id ", refNumber, amount, req.session.userDetails.username)

        var result = db.executeQuery(requestId,
            sql,
            function (error) {
                log.error(error);

                res.render('approval.html', {html_head_title: config.get('service.html_head_title'),
                    error_msg: "Request Failed. Please contact the administrator",
                    username: req.session.userDetails.username,
                    first_name: req.session.userDetails.first_name,
                    user_menus: req.session.pageMenus});
            },
            function (result) {
                var newApproval = result.rows[0];
                var errorMsg = "";
                if (newApproval) {
                    var reqId = newApproval.record_id;

                    errorMsg = util.format('Req #%s. New Topup approval for %s successfully created.', reqId, amount)
                }
                else {
                    errorMsg = "Invalid Transaction parameters"
                }

                res.render('approval.html', {html_head_title: config.get('service.html_head_title'),
                    error_msg: errorMsg,
                    username: req.session.userDetails.username,
                    first_name: req.session.userDetails.first_name,
                    user_menus: req.session.pageMenus});
            });
    }
    else {
        res.render('approval.html', {html_head_title: config.get('service.html_head_title'),
            error_msg: "Invalid Transaction parameters",
            username: req.session.userDetails.username,
            first_name: req.session.userDetails.first_name,
            user_menus: req.session.pageMenus});
    }

});

router.get('/topup', function(req, res, next) {
    //var isValid, errorMsg, redirect; ({isValid, errorMsg, redirect} = session.validateSession(req));

    if (req.session.userDetails == null) {
        res.render('login.html', {html_head_title: config.get('service.html_head_title'), error_msg: ""});
    }
    else if (req.session.userLinks.includes(req.originalUrl)) {

        // get pending approval requests:
        var sql = util.format("select * from tbl_vtop_topup_requests where status = 2 order by request_time desc");

        var result = db.executeQuery(1,
            sql,
            function(error){
                log.error (error);

                res.render('topup.html', {html_head_title: config.get('service.html_head_title'),
                    error_msg: "Error fetching approvals",
                    username: req.session.userDetails.username,
                    first_name: req.session.userDetails.first_name,
                    user_menus: req.session.pageMenus,
                    hidden: true
                });
            },

            function (result) {
                var pendingApprovals = result.rows;
                var approvalOptions = '<select class="custom-select d-block w-100" name="ref_no" id="ref_no" >';
                approvalOptions += '<option value="">Reference Number...</option>';

                for (let i in pendingApprovals) {
                    var approval = pendingApprovals[i];
                    approvalOptions += util.format('<option id="%s">%s</option>', approval.record_id, approval.reference_no);
                }

                approvalOptions += '</select>';

                res.render('topup.html', {html_head_title: config.get('service.html_head_title'),
                    error_msg: "",
                    username: req.session.userDetails.username,
                    first_name: req.session.userDetails.first_name,
                    user_menus: req.session.pageMenus,
                    hidden: false,
                    reference_options: approvalOptions
                });

            });
    }
    else{

        // take him back home
        res.redirect('/vtop');
    }
});

router.post('/topup', function(req, res, next) {

    var startTime = new Date().getTime();
    var random = util.format('%s', gen());
    var refNumber = req.body.ref_no;
    var amount = req.body.amount;
    var type = req.body.topup_type;

    var requestId = util.format('%d%d', startTime, random.padStart(4,0));

    if (req.session.userDetails == null) {
        res.render('login.html', {html_head_title: config.get('service.html_head_title'), error_msg: ""});
    }
    else if (refNumber != null && amount != null && type != null){
        apigw.topUp({refNumber: querystring.escape(refNumber),
            date: dateFormat(new Date(), "yyyy-mm-dd"),
            type: type,
            amount: amount,
            requestId: requestId,
            requestTime: startTime}, req, res);
    }
    else {
        res.render('topup.html',
            {html_head_title: config.get('service.html_head_title'),
            error_msg: "Invalid Transaction parameters",
                username: req.session.userDetails.username,
                first_name: req.session.userDetails.first_name,
                user_menus: req.session.pageMenus,
                hidden: true,
                reference_options: ''});
    }

});



// get data
router.post('/logout', function(req, res, next) {
    res.session = null;
});

// get data
router.post('/change_password', function(req, res, next) {
    //var isValid, errorMsg, redirect; ({isValid, errorMsg, redirect} = session.validateSession(req));

    var startTime = new Date().getTime();
    var random = util.format('%s', gen());
    var userId = req.body.user_id;
    var newPassword = req.body.newpassword;
    var oldPassword = req.body.oldpassword;
    var errorMsg = "";

    if (req.session.userDetails == null) {
        res.render('login.html', {html_head_title: config.get('service.html_head_title'), error_msg: ""});
    }
    else if (newPassword != '' && oldPassword != ''){

        var sql = util.format("UPDATE public.tbl_users SET password = '%s'::text WHERE user_id = '%s'::integer and password = '%s'::text",
            crypto.createHash('md5').update(querystring.escape(newPassword)).digest('hex'),
            userId,
            crypto.createHash('md5').update(querystring.escape(oldPassword)).digest('hex'));

        var result = db.executeQuery(1,
            sql,
            function (error) {
                log.error(error);

                // take him back home
                res.redirect('/vtop');
            },
            function (result) {
                var updates = result.rowCount;
                var errorMsg = "";
                if (updates == 1) {
                    errorMsg = "Password Successfully updates";
                    res.redirect('/vtop');

                }
                else {
                    req.session.errorMsg = "Password change failed! ";
                    res.redirect('/vtop');

                }
            });
    }
    else {
        req.session.errorMsg = "An error occured while processing you request. Please try again.";
        res.redirect('/vtop');
    }

});

router.get('/get_topups', function(req, res, next) {
    var startDate = req.query.from;
    var endDate = req.query.to;
    var searchValue = req.query.search.value;

    var queryString = " where notes like '\%" + searchValue + "\%' ";

    if (startDate != null || endDate != null) {
        queryString += util.format(" and topup_date::date between '%s' and '%s' ", startDate, endDate);
    }

    // get total records
    var totalRecordSql = util.format("select count(*) total from tbl_vtop_topups %s", queryString);

    db.executeQuery(1,
        totalRecordSql,
    function (err) {
        console.log(err);
        res.send(JSON.stringify({status: "nok"}));
    },
    function (totalRecords) {
        var total = totalRecords.rows;
        var totalsRow = total[0];
        var recordsTotal = totalsRow.total;
        // pagination
        queryString += util.format(' order by topup_date desc limit %s offset %s', req.query.length, req.query.start);

        var sql = util.format("select record_id, reference_no, topup_date, round(amount/100,2) amount, notes, entered_by, \n" +
            "       entered_at from tbl_vtop_topups %s", queryString);

        db.executeQuery(1,
            sql,
            function (err) {
                console.log(err);
                res.send(JSON.stringify({status: "nok"}));
            },
            function (result) {
                var results = result.rows;
                var recordsFiltered = recordsTotal;

                if (results) {
                    // fetch user permissions

                    res.send(JSON.stringify(
                        {draw: parseInt(req.query.draw),
                            recordsTotal: recordsTotal,
                            recordsFiltered: recordsFiltered,
                            data: results}));
                }
                else {
                    res.send(JSON.stringify({status: "nok"}));
                }
            });
    });


});

router.get('/get_recycles', function(req, res, next) {
    var startDate = req.query.from;
    var endDate = req.query.to;
    var queryString = '';

    if (startDate != null && endDate != null) {
        queryString = util.format(" and topup_date::date between '%s' and '%s' ", startDate, endDate);
    }

    var sql = util.format("select to_char(topup_date, 'DD/MM') recycle_date, round(sum(amount/100),2) recycle_amount from tbl_vtop_topups where notes = 'recycle' %s group by topup_date order by topup_date asc;", queryString);

    db.executeQuery(1,
        sql,
        function (err) {
            console.log(err);
            res.send(JSON.stringify({status: "nok"}));
        },
        function (result) {
            //console.log(result);

            var results = result.rows;

            if (results) {
                // fetch user permissions

                res.send(JSON.stringify({status: "OK", data: results}));
            }
            else {
                res.send(JSON.stringify({status: "nok"}));
            }
        });


});

router.get('/get_balance', function(req, res, next) {
    var endDate = req.query.date;

    var queryString = "";

    if (endDate != null) {
        queryString = util.format(" where loan_date::date <=  '%s'::date ", endDate);
    }

    var sql = util.format("select round(((sum(COALESCE(topup_amount, 0)) - (sum(COALESCE(princ_loaned, 0) - COALESCE(recycled_amount, 0)))) / sum(COALESCE(topup_amount, 0)))*100, 2) as float_level, \n" +
        "\t   round((sum(COALESCE(topup_amount, 0)) - (sum(COALESCE(princ_loaned, 0) - COALESCE(recycled_amount, 0))))/100, 2) as current_balance,  \n" +
        "\t   round(sum(COALESCE(topup_amount, 0)/100), 2) as float_topups \n" +
        "from vw_float_movement %s", queryString);

    db.executeQuery(1,
        sql,
        function (err) {
            console.log(err);
            res.send(JSON.stringify({status: "nok"}));
        },
        function (result) {
            //console.log(result);

            var results = result.rows;

            if (results) {
                // fetch user permissions

                res.send(JSON.stringify({status: "OK", data: results}));
            }
            else {
                res.send(JSON.stringify({status: "nok"}));
            }
        });


});

router.get('/get_approval_info', function(req, res, next) {

    var sql = util.format("select * from tbl_vtop_topup_requests where record_id = %s::bigint", req.query.record_id);

    db.executeQuery(1,
        sql,
        function (err) {
            console.log(err);
            res.send(JSON.stringify({status: "nok"}));
        },
        function (result) {
            //console.log(result);

            var results = result.rows;

            if (results) {
                // save in session data
                req.session.topup_approval_info = results[0];
                res.send(JSON.stringify({status: "OK", data: results}));
            }
            else {
                res.send(JSON.stringify({status: "nok"}));
            }
        });


});

router.get('/users', function(req, res, next) {
    //var isValid, errorMsg, redirect; ({isValid, errorMsg, redirect} = session.validateSession(req));

    if (req.session.userDetails == null) {

        res.render('login.html', {html_head_title: config.get('service.html_head_title'), error_msg: ""});
    }
    else if (req.session.userLinks.includes(req.originalUrl)) {

        if (req.session.vtop_user_roles != null) {

            res.render('users.html', {html_head_title: config.get('service.html_head_title'),
                error_msg: "",
                username: req.session.userDetails.username,
                first_name: req.session.userDetails.first_name,
                user_menus: req.session.pageMenus,
                user_roles_options: req.session.vtop_user_roles});
        }
        else {
            // get pending approval requests:
            var sql = util.format("select role_id, role_name from tbl_roles order by role_id asc");

            var result = db.executeQuery(1,
                sql,
                function(error){
                    log.error (error);
                    res.render('users.html', {html_head_title: config.get('service.html_head_title'),
                        error_msg: "",
                        username: req.session.userDetails.username,
                        first_name: req.session.userDetails.first_name,
                        user_menus: req.session.pageMenus,
                        user_roles_options: req.session.vtop_user_roles});
                },
                function (result) {
                    var user_roles_options = '';

                    for (let i in result.rows) {
                        var role = result.rows[i];
                        user_roles_options += util.format('<option value="%s">%s</option>', role.role_id, role.role_name);
                    }

                    // persist in session store so that we don't query the DB next time
                    req.session.vtop_user_roles = user_roles_options;

                    res.render('users.html', {html_head_title: config.get('service.html_head_title'),
                        error_msg: "",
                        username: req.session.userDetails.username,
                        first_name: req.session.userDetails.first_name,
                        user_menus: req.session.pageMenus,
                        user_roles_options: user_roles_options});
                });
        }
    }
    else{

        // take him back home
        res.redirect('/vtop');
    }
});

router.get('/get_users', function(req, res, next) {

    var searchValue = req.query.search.value;

    var queryString = " where username like '\%" + searchValue + "\%' ";

    // get total records
    var totalRecordSql = util.format("select count(*) total from tbl_users %s", queryString);

    db.executeQuery(1,
        totalRecordSql,
        function (err) {
            console.log(err);
            res.send(JSON.stringify({status: "nok"}));
        },
        function (totalRecords) {
            var total = totalRecords.rows;
            var totalsRow = total[0];
            var recordsTotal = totalsRow.total;
            // pagination
            queryString += util.format(' group by t1.user_id, username, first_name, middle_name, last_name, status, create_date, last_login order by create_date desc limit %s offset %s', req.query.length, req.query.start);

            var sql = util.format("select t1.user_id, string_agg(t2.role_id::text, ',') roles, username, first_name, middle_name, last_name, status, create_date, last_login \n" +
                "from tbl_users t1 left join tbl_user_roles t2 \n" +
                "on t1.user_id = t2.user_id %s ", queryString);

            db.executeQuery(1,
                sql,
                function (err) {
                    console.log(err);
                    res.send(JSON.stringify({status: "nok"}));
                },
                function (result) {
                    var results = result.rows;
                    var recordsFiltered = recordsTotal;

                    if (results) {
                        // fetch user permissions

                        res.send(JSON.stringify(
                            {draw: parseInt(req.query.draw),
                                recordsTotal: recordsTotal,
                                recordsFiltered: recordsFiltered,
                                data: results}));
                    }
                    else {
                        res.send(JSON.stringify({status: "nok"}));
                    }
                });
        });


});

router.post('/add_user', function(req, res, next) {
    //var isValid, errorMsg, redirect; ({isValid, errorMsg, redirect} = session.validateSession(req));

    console.log(req);

    var startTime = new Date().getTime();
    var random = util.format('%s', gen());
    var userName = req.body.username;
    var password = req.body.password;
    var firstName = req.body.first_name;
    var middleName = req.body.middle_name;
    var lastName = req.body.last_name;
    var roleId = req.body.role;

    if (req.session.userDetails == null) {
        res.render('login.html', {html_head_title: config.get('service.html_head_title'), error_msg: ""});
    }
    else if (userName != '' &&
            password != '' &&
            firstName != '' &&
            middleName != '' &&
            lastName != '' &&
            roleId != ''){
        var sql = util.format("INSERT INTO public.tbl_users(\n" +
            "\tusername, password, first_name, middle_name, last_name, status)\n" +
            "\tVALUES ('%s'::text, '%s'::text, '%s'::text, '%s'::text, '%s'::text, '%s'::integer) returning user_id",
            querystring.escape(userName),
            crypto.createHash('md5').update(querystring.escape(password)).digest('hex'),
            querystring.escape(firstName),
            querystring.escape(middleName),
            querystring.escape(lastName),
            1)

        var result = db.executeQuery(1,
            sql,
            function (error) {
                log.error(error);

                res.render('users.html', {html_head_title: config.get('service.html_head_title'),
                    error_msg: "Request Failed. Please contact the administrator",
                    username: req.session.userDetails.username,
                    first_name: req.session.userDetails.first_name,
                    user_menus: req.session.pageMenus});
            },
            function (result) {
                var newUser = result.rows[0];
                var errorMsg = "";
                if (newUser) {
                    var userId = newUser.user_id;

                    // add user roles
                    var userRoleSQL = util.format("INSERT INTO public.tbl_user_roles(role_id, user_id) VALUES ('%s'::integer, '%s'::integer)",
                        querystring.escape(roleId),
                        querystring.escape(userId));

                    db.executeQuery(1,
                        userRoleSQL,
                        function (error) {
                            log.error(error);

                            res.render('users.html', {html_head_title: config.get('service.html_head_title'),
                                error_msg: "Request Failed. Please contact the administrator",
                                username: req.session.userDetails.username,
                                first_name: req.session.userDetails.first_name,
                                user_menus: req.session.pageMenus});
                        },
                        function (result) {
                            res.render('users.html', {html_head_title: config.get('service.html_head_title'),
                                error_msg: errorMsg,
                                username: req.session.userDetails.username,
                                first_name: req.session.userDetails.first_name,
                                user_menus: req.session.pageMenus});
                        });
                }
                else {
                    errorMsg = "User successfully created";

                    res.render('users.html', {html_head_title: config.get('service.html_head_title'),
                        error_msg: errorMsg,
                        username: req.session.userDetails.username,
                        first_name: req.session.userDetails.first_name,
                        user_menus: req.session.pageMenus});
                }
            });
    }

});

router.post('/update_user', function(req, res, next) {
    //var isValid, errorMsg, redirect; ({isValid, errorMsg, redirect} = session.validateSession(req));

    console.log(req);

    var startTime = new Date().getTime();
    var random = util.format('%s', gen());
    var userId = req.body.user_id;
    var password = req.body.password;
    var firstName = req.body.first_name;
    var middleName = req.body.middle_name;
    var lastName = req.body.last_name;
    var status = req.body.user_status;
    var roleId = req.body.role;
    var errorMsg = "";

    if (req.session.userDetails == null) {
        res.render('login.html', {html_head_title: config.get('service.html_head_title'), error_msg: ""});
    }
    else if (password != '' &&
        firstName != '' &&
        middleName != '' &&
        lastName != '' &&
        roleId != ''){
        var sql = util.format("UPDATE public.tbl_users SET\n" +
            "\tpassword = '%s'::text, first_name = '%s'::text, middle_name = '%s'::text, last_name = '%s'::text, status = '%s'::integer\n" +
            "\tWHERE user_id = '%s'::integer",
            crypto.createHash('md5').update(querystring.escape(password)).digest('hex'),
            querystring.escape(firstName),
            querystring.escape(middleName),
            querystring.escape(lastName),
            querystring.escape(status),
            userId);

        var result = db.executeQuery(1,
            sql,
            function (error) {
                log.error(error);

                res.render('users.html', {html_head_title: config.get('service.html_head_title'),
                    error_msg: "Request Failed. Please contact the administrator",
                    username: req.session.userDetails.username,
                    first_name: req.session.userDetails.first_name,
                    user_menus: req.session.pageMenus});
            },
            function (result) {
                var updates = result.rowCount;
                var errorMsg = "";
                if (updates == 1) {
                   // add user roles
                    var userRoleSQL = util.format("UPDATE public.tbl_user_roles set role_id = '%s'::integer where user_id = '%s'::integer",
                        querystring.escape(roleId),
                        querystring.escape(userId));

                    db.executeQuery(1,
                        userRoleSQL,
                        function (error) {
                            log.error(error);

                            res.render('users.html', {html_head_title: config.get('service.html_head_title'),
                                error_msg: "Request Failed. Please contact the administrator",
                                username: req.session.userDetails.username,
                                first_name: req.session.userDetails.first_name,
                                user_menus: req.session.pageMenus});
                        },
                        function (r) {
                            if (r.rowCount != 1) {

                                // insert new role;
                                db.executeQuery(1,
                                    util.format("INSERT INTO public.tbl_user_roles (role_id, user_id) VALUES ('%s'::integer, '%s'::integer)",
                                        querystring.escape(roleId),
                                        querystring.escape(userId)),
                                    function (error) {
                                        errorMsg = "User role profile not updated";
                                        res.render('users.html', {html_head_title: config.get('service.html_head_title'),
                                            error_msg: errorMsg,
                                            username: req.session.userDetails.username,
                                            first_name: req.session.userDetails.first_name,
                                            user_menus: req.session.pageMenus});
                                    },
                                    function (reslt) {
                                        errorMsg = "User Profile successfully updated";
                                        res.render('users.html', {html_head_title: config.get('service.html_head_title'),
                                            error_msg: errorMsg,
                                            username: req.session.userDetails.username,
                                            first_name: req.session.userDetails.first_name,
                                            user_menus: req.session.pageMenus});
                                    });
                            } else {
                                errorMsg = "User role profile successfully updated";

                                res.render('users.html', {html_head_title: config.get('service.html_head_title'),
                                    error_msg: errorMsg,
                                    username: req.session.userDetails.username,
                                    first_name: req.session.userDetails.first_name,
                                    user_menus: req.session.pageMenus});
                            }
                        });
                }
                else {
                    errorMsg = "User Profile Update failed! ";

                    res.render('users.html', {html_head_title: config.get('service.html_head_title'),
                        error_msg: errorMsg,
                        username: req.session.userDetails.username,
                        first_name: req.session.userDetails.first_name,
                        user_menus: req.session.pageMenus});
                }
            });
    }

});

router.get("/error", function(req, res, next) {
    res.render('error.html', {result: req.query.message});
});

module.exports = router;
