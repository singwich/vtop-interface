/* load and validate configurations*/
var convict = require('convict');
var toml = require('toml');
var log = require('../common/logging');
var util = require('util');

// our config schema
var schema =
    {redis: {
        host: {format: "ipaddress", default: "127.0.0.1"},
            port: {format: "port", default: 12345},
            ttl: {format: "int", default: 120}
        },

    database: {
        connection_url: {format: "*", default: "postgres://localhost:5432/vtop"},
        host: {format: "ipaddress", default: "127.0.0.1"},
        port: {format: "port", default: 5432},
        dbname: {format: "*", default: "vtop"},
        user: {format: "*", default: "postgres"},
        password: {format: "*", default: "postgres"},
        min_pool_size: {format: "int", default: 10},
        max_pool_size: {format: "int", default: 10}},

    vtop: {
        topup_url: {format: "url", default: "http://127.0.0.1:8282/vtop5/topup"},
        topup_timeout: {format: "int", default: 60000},
        reversal_url: {format: "url", default: "http://127.0.0.1:8282/vtop5/reverse"},
        reversal_timeout: {format: "int", default: 60000}},
    service: {
        http_port: {format: "port", default: 3000},
        html_head_title: {format: "*", default: "Mascom FMI"}
    }};

var config = convict(schema);
convict.addParser({ extension: 'properties', parse: toml.parse });

try {
    var configFile = process.env.CONFIG || './config/config.properties'
    log.info(util.format("config|operation=loadFile|file='%s'", configFile));
    config.loadFile(configFile);
    log.info("config|operation=loadFile|status='completed successfully'");

    log.info('config|operation=validate');
    // Perform validation
    config.validate({allowed: 'strict'});
    log.info("config|operation=validate|status='completed successfully'");
}
catch (e) {
    log.error(util.format("!config|error=%s", e.messge));

    throw e;
}


module.exports = config;
