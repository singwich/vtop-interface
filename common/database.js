/*
*
*   SQL stuff here.
*
*/
const pg = require('pg');
var log = require('../common/logging');
var config = require('../common/config');
var util = require('util');

var client = null;
var pool = null;

function initialize(){
    var connectionURL = util.format("postgresql://%s:%s@%s:%s/%s",
        config.get("database.user"),
        config.get("database.password"),
        config.get("database.host"),
        config.get("database.port"),
        config.get("database.dbname"));

    var maskedConnectionURL = util.format("postgresql://%s:*******@%s:%s/%s",
        config.get("database.user"),
        config.get("database.host"),
        config.get("database.port"),
        config.get("database.dbname"));

    this.client = new pg.Client(connectionURL);

    try {
        log.info(util.format("initializeDBConnection|url=%s", maskedConnectionURL));
        this.client.connect();
        log.info("initializeDBConnection|status='connection successfully established'");
    }
    catch (e) {
        log.error(util.format("initializeDBConnection|status='failed'|error=%s", connectionURL, e.message));

        throw e;
    }
}

function executeQuery(id, sql, handleErrorFn, handleResultFn) {
    try {
        log.debug(util.format("event='executeQuery'|sessionId=%s|query=%s", id, sql));
        const query = this.client.query(sql, (err, res) => {

            if (err) {
                handleErrorFn(err);
            }
            else if (res) {
                handleResultFn(res);
            }
            else handleErrorFn(err);
        });
        //this.client.end();
    }
    catch (e) {
        log.error(util.format("event='executeQuery'|sessionId=%s|status='failed'|error=%s", id, e.message))

        throw  e;
        //return {status: "nok", error: e.message}
    }
}

function shutDown(){
    try {
        this.client.end();
    }
    catch (e) {
        log.error(util.format("event='shutDownDBConnection'|status='failed'|error=%s", e.message))

        return {status: "nok", error: e.message}
    }
}

module.exports = {initialize, executeQuery, shutDown}
