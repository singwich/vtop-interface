var log = require('../common/logging');
var util = require('util');
const { base64encode, base64decode } = require('nodejs-base64');

function sessionHandler(req, res, next) {
    if (req.session.msisdn) {
        var sessionId = req.session.sessionId;
        var msisdn = req.query.subscriber_no;

        // validate session
        if (sessionId == req.query.id || sessionId == req.body.sessionId) {
            log.info(util.format('getSession|sessionId=%s|data=%s', sessionId, req.session));

            next();
        }
        else {
            var errorMsg = util.format('sessionMismatch|expected=%s|found=%s', req.query.sessionId, sessionId)
            log.error(errorMsg);

            res.render('error.html', {error: "Invalid Request", error_msg: errorMsg});
        }
    } else {
        log.info(util.format('createNewSession|sessionId=%s|msisdn=%s', req.query.id, req.query.subscriber_no));

        req.session.msisdn = req.query.subscriber_no;
        req.session.sessionId = req.query.id;

        //console.log(req.session);

        next();
    }
}
//
// Destroy session
function destroySession(sessionId, req){
    try {
        req.session.destroy(function(err){
            if(err){
                log.debug(util.format("destroySessionError|sessionId=%s|error=%s", sessionId, err));
                return false;
            } else {
                return true;
            }
        });

        return true;
    }
    catch (e) {
        log.error(util.format("!destroySession|sessionId=%s|error=%s", sessionId, e));

        return false;
    }
}

function validateSession(req) {
    if (req.session.msisdn) {
        var sessionId = req.sessionID;
        var msisdn = req.query.subscriber_no;

        // validate MSISDN
        if (msisdn != null && !isValidMSISDN(base64decode(msisdn))) {
            return {isValid: false, errorMsg: "Invalid MSISDN format."}
        }

        // validate session
        else if (sessionId == req.query.id || sessionId == req.body.sessionId) {
            log.info(util.format('getSession|sessionId=%s|data=%s', sessionId, req.session));

            return {isValid: true};
        }
        else {
            var errorMsg = util.format('sessionMismatch|expected=%s|found=%s', req.query.sessionId, sessionId);
            log.error(errorMsg);


            req.session.regenerate(function (err) {
                log.debug('regenerateSession: ', err);
            });

            return {isValid: true, errorMsg: errorMsg, redirect: true};
        }
    } else {
        log.info(util.format('createNewSession|sessionId=%s|msisdn=%s', req.query.id, req.query.subscriber_no));

        req.session.msisdn = req.query.subscriber_no;
        req.session.sessionId = req.query.id;

        // console.log(req);

        return {isValid: true};
    }

    return {isValid: true};
}

function isValidMSISDN(msisdn) {
    return /^\d+$/.test(msisdn);
}

module.exports = {validateSession, isValidMSISDN}