// Digital Exhaust stuff. This my way of telling the outer world what is happening in my inner world
// Listen to what I am saying and we shall all be good. Sawa? Good!

var appRoot = require('app-root-path');
var winston = require('winston');

// file and console transports configuration settings
var options = {
    file: {
        level: 'info',
        filename: `${appRoot}/logs/app.log`,
        handleExceptions: true,
        json: false,
        maxsize: 5242880, // 5MB
        maxFiles: 5,
        colorize: false,
        format: winston.format.combine(
            winston.format.timestamp(),
            winston.format.printf(info => {
                return `${info.timestamp} level=${info.level}|${info.message}`;
            })
        )
    },
    console: {
        level: 'debug',
        handleExceptions: true,
        json: false,
        colorize: true,
        format: winston.format.combine(
            winston.format.timestamp(),
            winston.format.printf(info => {
                return `${info.timestamp} level=${info.level}|${info.message}`;
            })
        )
    },
};

// instantiate a new winston logger
var logger = winston.createLogger({
    transports: [
        new winston.transports.File(options.file),
        new winston.transports.Console(options.console)
    ],
    exitOnError: false, // do not exit on handled exceptions
});

// define a stream function for morgan outputs
logger.stream = {
    write: function(message, encoding) {
        logger.info(message);
    },
};

// make this logger available to the rest of the world
module.exports = logger;

