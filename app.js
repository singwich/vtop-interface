var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var morgan = require('morgan');
var session = require('express-session');
var util = require('util');
var compression = require('compression')

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var vtopRouter = require('./routes/vtop');
var config = require('./common/config');
var winston = require('./common/logging');
var sess = require('./common/session');
var db = require('./common/database');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.engine('html', require('ejs').renderFile);
app.use(morgan('httpMethod=:method|url=:url|httpStatus=:status|contentLength=:res[content-length]|responseTime=:response-time ms', {stream: winston.stream}));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
//app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(compression());
// session store
app.use(session({
  secret: '&0mk03143s@a234234a0',
  cookie: {
      cookieName: 'xyz',
      expires: true,
      httpOnly: false,
      secret: 'kut1n007',
      secure: false,
      maxAge: 600000
  },
  saveUninitialized: false,
  resave: true,
  //genid: function (req) {
  //    return req.query.id;
  //},

}));
app.use(compression());

app.use('/vtop', vtopRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // add this line to include winston logging
  console.log(err);
  winston.error(`${err.status || 500} - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`);

  // render the error page
  var errorMsg = (err.status === 404) ? "Sorry, we couldn't find the page you are looking for." : "An error occurred while processing your request. Please try again later."
  res.status(err.status || 500);
  res.render('error.html', {html_head_title: config.get('service.html_head_title'), result: errorMsg})
});

// initialize DB
db.initialize();

module.exports = app;
